# Debugger

- Debugging tool
- Kompiliert mit den entsprechendem Funktionsumfang (Siehe die Flags)
- Maske mit aktivierten flags
- FATAL macht automatisch einen SIGABRT --> corefile

## Benutzung
- DEBUG(v1, ...): Nur bei der kompilierung mit dem QT_DEBUG flag aktiviert
- INFO(v1, ...): Nur bei der kompilierung mit dem QT_DEBUG flag aktiviert
- WARNING(v1, ...): Nur bei der kompilierung mit dem QT_DEBUG flag aktiviert
- CRITICAL(v1, ...): Nur bei der kompilierung mit dem QT_DEBUG flag aktiviert
- FATAL(v1, v2, ...): Bei der Kompilierung mit QT_DEBUG oder QT_RELEASE Aktiviert

## Flags
- DEBUG_TO_TERMINAL       --> Debugging output to terminal
- DEBUG_TO_FILE           --> Debugging output to file
- DEBUG_TO_ERROR          --> Debugging to stderr

- DEBUG_ADD_FILE
- DEBUG_ADD_LINE
- DEBUG_ADD_FUNCTION
- DEBUG_ADD_TIME
- DEBUG_ADD_PID

- DEBUG_DEBUG
- DEBUG_INFO
- DEBUG_WARNING
- DEBUG_CRITICAL
- DEBUG_FATAL

- DEBUG
- WARN
- FATAL <> ASSERT
- BREXIT
- void Error


## TODO
- Farben???
