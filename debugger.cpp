
#include "debugger.h"

Debugger::OutType Debugger::out_type;
QTemporaryFile* Debugger::file_to_write = nullptr;
QTextStream* Debugger::term_to_write = nullptr;
QTextStream* Debugger::err_to_write = nullptr;
QTextStream* Debugger::f = nullptr;
char Debugger::s1[BUFFER_SIZE];
QString Debugger::s2;

void __attribute__ ((constructor)) InitDebugger(void)
{
	qDebug("------------------------------------------------ Debugger is initialized ------------------------------------------------"); 


	qInstallMessageHandler(Debugger::MessageHandler);

#ifdef DEBUG_TO_FILE
#ifdef _WIN32
	char* s = new char[1024];
	ExpandEnvironmentStringsA( "%LOCALAPPDATA%\\Temp\\ut-XXXXXXXX.log", s, 1024);
#else
	const char* s;
	s = "/tmp/ut-XXXXXXXX.log";
#endif

	 Debugger::file_to_write = new QTemporaryFile();
	 Debugger::file_to_write->setAutoRemove(false);
	 Debugger::file_to_write->setFileTemplate(QString(s));

	 if (!Debugger::file_to_write->open())
	 	 exit(1);
	 printf("Debugging file: %s\n", Debugger::file_to_write->fileName().toLocal8Bit().data());

	 Debugger::f = new QTextStream(Debugger::file_to_write);

#ifdef _WIN32
	 delete[] s;
#endif
#endif 

#ifdef DEBUG_TO_TERMINAL
	 Debugger::term_to_write = new QTextStream(stdout);
#endif

#ifdef DEBUG_TO_ERROR
	 Debugger::err_to_write = new QTextStream(stderr);
#endif


	

#ifdef DEBUG_DEBUG
	 Debugger::out_type = static_cast<Debugger::OutType>(Debugger::out_type | Debugger::OUT_DEBUG);
#endif
#ifdef DEBUG_INFO
	 Debugger::out_type = static_cast<Debugger::OutType>(Debugger::out_type | Debugger::OUT_INFO);
#endif
#ifdef DEBUG_WARNING
	 Debugger::out_type = static_cast<Debugger::OutType>(Debugger::out_type | Debugger::OUT_WARNING);
#endif
#ifdef DEBUG_CRITICAL
	 Debugger::out_type = static_cast<Debugger::OutType>(Debugger::out_type | Debugger::OUT_CRITICAL);
#endif
#ifdef DEBUG_FATAL
	 Debugger::out_type = static_cast<Debugger::OutType>(Debugger::out_type | Debugger::OUT_FATAL);
#endif
}

void __attribute__ ((destructor)) CleanUpDebugger(void)
{
	qInstallMessageHandler(nullptr);

	qInfo("------------------------------------------------ Debugger is exited ------------------------------------------------"); 
}





void Debugger::MessageHandler(QtMsgType _t, const QMessageLogContext& _c, const QString& _m)
{
	QByteArray lm;

	lm = _m.toLocal8Bit();

	switch (_t) {
		case QtDebugMsg:
#ifdef DEBUG_TO_TERMINAL
			*Debugger::term_to_write << DEBUG_COLOR;
			*Debugger::term_to_write << lm.constData();
			*Debugger::term_to_write << LINE_ENDING;
			Debugger::term_to_write->flush();
#endif

#ifdef DEBUG_TO_FILE
			*Debugger::f << "DEBUG:    ";
			*Debugger::f << lm.constData();
			*Debugger::f << LINE_ENDING;
			Debugger::f->flush();
#endif
			break;

		case QtInfoMsg:
#ifdef DEBUG_TO_TERMINAL
			*Debugger::term_to_write << INFO_COLOR;
			*Debugger::term_to_write << lm.constData();
			*Debugger::term_to_write << LINE_ENDING;
			Debugger::term_to_write->flush();
#endif

#ifdef DEBUG_TO_FILE
			*Debugger::f << "INFO:     ";
			*Debugger::f << lm.constData();
			*Debugger::f << LINE_ENDING;
			Debugger::f->flush();
#endif
			break;

		case QtWarningMsg:
#ifdef DEBUG_TO_TERMINAL
			*Debugger::term_to_write << WARNING_COLOR;
			*Debugger::term_to_write << lm.constData();
			*Debugger::term_to_write << LINE_ENDING;
			Debugger::term_to_write->flush();
#endif

#ifdef DEBUG_TO_FILE
			*Debugger::f << "WARNING:  ";
			*Debugger::f << lm.constData();
			*Debugger::f << LINE_ENDING;
			Debugger::f->flush();
#endif
			break;

		case QtCriticalMsg:
#ifdef DEBUG_TO_TERMINAL
			*Debugger::term_to_write << CRITICAL_COLOR;
			*Debugger::term_to_write << lm.constData();
			*Debugger::term_to_write << LINE_ENDING;
			Debugger::term_to_write->flush();
#endif

#if defined(DEBUG_TO_ERROR) and not defined(DEBUG_TO_TERMINAL)
			*Debugger::err_to_write << CRITICAL_COLOR;
			*Debugger::err_to_write << lm.constData();
			*Debugger::err_to_write << LINE_ENDING;
			Debugger::err_to_write->flush();
#endif

#ifdef DEBUG_TO_FILE
			*Debugger::f << "CRITICAL: ";
			*Debugger::f << lm.constData();
			*Debugger::f << LINE_ENDING;
			Debugger::f->flush();
#endif
			break;

		case QtFatalMsg:
 #ifdef DEBUG_TO_ERROR
 			*Debugger::err_to_write << FATAL_COLOR;
 			*Debugger::err_to_write << lm.constData();
 			*Debugger::err_to_write << LINE_ENDING;
 			Debugger::err_to_write->flush();
 #endif
 
#ifdef DEBUG_TO_FILE
			*Debugger::f << "FATAL:    ";
			*Debugger::f << lm.constData();
			*Debugger::f << LINE_ENDING;
			Debugger::f->flush();
#endif
			break;
	}
}




















void Debugger::Debug(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...)
{
#ifdef DEBUG_DEBUG
	va_list args;

	va_start(args, _s5);
	Debugger::s2.clear();

	Debugger::s2 += _s3;

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += _s1;

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += " (";
#endif
#endif

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += QString::asprintf("%d", _l);

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += ")";
#endif
#endif

#ifdef DEBUG_ADD_TIME
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += " at time {";
#endif

	Debugger::s2 += QDateTime::currentDateTime().toString();

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += "}";
#endif
#endif

#ifdef DEBUG_ADD_FUNCTION
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += " in ";
#endif
	Debugger::s2 += _s2;
#endif

#ifdef DEBUG_ADD_PID
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += " (PID: ";
#endif

	Debugger::s2 += QString::asprintf("%d", getpid());

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += ") ";
#endif
#endif

	Debugger::s2 += _s4;

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_FUNCTION) || defined(DEBUG_ADD_TIME) || defined(DEBUG_ADD_PID)
	Debugger::s2 += ": ";
#endif

	Debugger::s2 += QString::vasprintf(_s5, args);

	qDebug("%s", Debugger::s2.toLocal8Bit().data());

	va_end(args);
#endif
}

void Debugger::Info(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...)
{
#ifdef DEBUG_INFO
	va_list args;

	va_start(args, _s5);
	Debugger::s2.clear();

	Debugger::s2 += _s3;

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += _s1;

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += " (";
#endif
#endif

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += QString::asprintf("%d", _l);

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += ")";
#endif
#endif

#ifdef DEBUG_ADD_TIME
#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += " at time {";
#endif

	Debugger::s2 += QDateTime::currentDateTime().toString();

#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += "}";
#endif
#endif

#ifdef DEBUG_ADD_FUNCTION
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += " in ";
#endif
	Debugger::s2 += _s2;
#endif

#ifdef DEBUG_ADD_PID
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += " (PID: ";
#endif

	Debugger::s2 += QString::asprintf("%d", getpid());

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += ") ";
#endif
#endif

	Debugger::s2 += _s4;

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_FUNCTION) || defined(DEBUG_ADD_TIME) || defined(DEBUG_ADD_PID)
	Debugger::s2 += ": ";
#endif

	Debugger::s2 += QString::vasprintf(_s5, args);

	qInfo("%s", Debugger::s2.toLocal8Bit().data());

	va_end(args);
#endif
}


void Debugger::Warning(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...)
{
#ifdef DEBUG_WARNING
	va_list args;

	va_start(args, _s5);
	Debugger::s2.clear();

	Debugger::s2 += _s3;

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += _s1;

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += " (";
#endif
#endif

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += QString::asprintf("%d", _l);

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += ")";
#endif
#endif

#ifdef DEBUG_ADD_TIME
#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += " at time {";
#endif

	Debugger::s2 += QDateTime::currentDateTime().toString();

#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += "}";
#endif
#endif

#ifdef DEBUG_ADD_FUNCTION
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += " in ";
#endif
	Debugger::s2 += _s2;
#endif

#ifdef DEBUG_ADD_PID
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += " (PID: ";
#endif

	Debugger::s2 += QString::asprintf("%d", getpid());

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += ") ";
#endif
#endif

	Debugger::s2 += _s4;

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_FUNCTION) || defined(DEBUG_ADD_TIME) || defined(DEBUG_ADD_PID)
	Debugger::s2 += ": ";
#endif

	Debugger::s2 += QString::vasprintf(_s5, args);

	qWarning("%s", Debugger::s2.toLocal8Bit().data());

	va_end(args);
#endif
}


void Debugger::Critical(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...)
{
#ifdef DEBUG_CRITICAL
	va_list args;

	va_start(args, _s5);
	Debugger::s2.clear();

	Debugger::s2 += _s3;

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += _s1;

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += " (";
#endif
#endif

#ifdef DEBUG_ADD_LINE
	Debugger::s2 += QString::asprintf("%d", _l);

#ifdef DEBUG_ADD_FILE
	Debugger::s2 += ")";
#endif
#endif

#ifdef DEBUG_ADD_TIME
#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += " at time {";
#endif

	Debugger::s2 += QDateTime::currentDateTime().toString();

#if defined(DEBUG_ADD_FILE) or defined(DEBUG_ADD_LINE)
	Debugger::s2 += "}";
#endif
#endif

#ifdef DEBUG_ADD_FUNCTION
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE)
	Debugger::s2 += " in ";
#endif
	Debugger::s2 += _s2;
#endif

#ifdef DEBUG_ADD_PID
#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += " (PID: ";
#endif

	Debugger::s2 += QString::asprintf("%d", getpid());

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_TIME)
	Debugger::s2 += ") ";
#endif
#endif

	Debugger::s2 += _s4;

#if defined(DEBUG_ADD_FILE) || defined(DEBUG_ADD_LINE) || defined(DEBUG_ADD_FUNCTION) || defined(DEBUG_ADD_TIME) || defined(DEBUG_ADD_PID)
	Debugger::s2 += ": ";
#endif

	Debugger::s2 += QString::vasprintf(_s5, args);

	qCritical("%s", Debugger::s2.toLocal8Bit().data());

	va_end(args);
#endif
}


void Debugger::Fatal(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...)
{
#ifdef DEBUG_FATAL
	va_list args;

	va_start(args, _s5);

	qFatal("%sEpic Fail(%s): in %s (%s:%d) (PID: %d): %s%s", _s3, reinterpret_cast<char*>(QDateTime::currentDateTime().toString().toLocal8Bit().data()), _s2, _s1, _l, getpid(), _s4, reinterpret_cast<char*>(QString::vasprintf(_s5, args).toLocal8Bit().data()));

	va_end(args);
#endif
}

