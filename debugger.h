
#include <QtGlobal>
#include <QByteArray>
#include <QString>
#include <QTemporaryFile>
#include <QTextStream>
#include <QDateTime>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define ANSI_RESET                                   "\u001b[0m"

#define ANSI_TEXT_RED                                "\u001b[31m"
#define ANSI_TEXT_BLUE                               "\u001b[34m"
#define ANSI_TEXT_YELLOW                             "\u001b[33m"
#define ANSI_TEXT_GREEN                              "\u001b[32m"
#define ANSI_TEXT_BLACK                              "\u001b[30m"
#define ANSI_TEXT_CYAN                               "\u001b[36m"
#define ANSI_TEXT_MAGENTA                            "\u001b[35m"
#define ANSI_TEXT_WHITE                              "\u001b[37m"

#define ANSI_BACKGROUND_RED                          "\u001b[41m"
#define ANSI_BACKGROUND_BLUE                         "\u001b[44m"
#define ANSI_BACKGROUND_YELLOW                       "\u001b[43m"
#define ANSI_BACKGROUND_GREEN                        "\u001b[42m"
#define ANSI_BACKGROUND_BLACK                        "\u001b[40m"
#define ANSI_BACKGROUND_CYAN                         "\u001b[46m"
#define ANSI_BACKGROUND_MAGENTA                      "\u001b[45m"
#define ANSI_BACKGROUND_WHITE                        "\u001b[47m"

#define COLOR_EXTRA_INFORMATION                      ANSI_RESET

#define MAX_FILENAME_LENGTH                          8192

#ifdef DEBUG_ADD_ALL
#define DEBUG_ADD_LINE
#define DEBUG_ADD_FILE
#define DEBUG_ADD_FUNCTION
#define DEBUG_ADD_TIME
#define DEBUG_ADD_PID
#endif

#ifdef DEBUG_TO_ALL
#define DEBUG_TO_FILE
#define DEBUG_TO_TERMINAL
#define DEBUG_TO_ERROR
#endif

#ifdef DEBUG_ENABLE_ALL
#define DEBUG_DEBUG
#define DEBUG_INFO
#define DEBUG_WARNING
#define DEBUG_CRITICAL
#define DEBUG_FATAL
#endif


#ifdef QT_DEBUG
#define DEBUG(v1, ...)                  Debugger::Debug(__FILE__, __LINE__, __PRETTY_FUNCTION__, "", "", v1, ##__VA_ARGS__)
#define INFO(v1, ...)                   Debugger::Info(__FILE__, __LINE__, __PRETTY_FUNCTION__, "", "", v1, ##__VA_ARGS__)
#define WARNING(v1, ...)                Debugger::Warning(__FILE__, __LINE__, __PRETTY_FUNCTION__, "", "", v1, ##__VA_ARGS__)
#define CRITICAL(v1, ...)               Debugger::Critical(__FILE__, __LINE__, __PRETTY_FUNCTION__, "", "", v1, ##__VA_ARGS__)
#define FATAL(v1, v2, ...)              { Debugger::Fatal(__FILE__, __LINE__, __PRETTY_FUNCTION__, "", "", v1, ##__VA_ARGS__); exit(v1); }

#define FUNCTIONCHECK(v1, v2)           \
({\
	auto s = v2;\
   if (v1 != s)\
		WARN("Function %s did not work properly\n", #v2);\
	s;\
})
#define BREXIT(v)                     { DEBUG("OH NO!!!!!!!!!!!!! something went wrong here\n"); exit(v); }
#define DIE                           *((char*)0) = 0

#else

#define DEBUG
#define INFO
#define WARNING
#define CRITICAL
#define FATAL(v1, v2, ...)            { exit(v1); }

#define FUNCTIONCHECK(v1, v2)         v2
#define BREXIT(v)                     exit(v)
#define DIE
#endif

#ifdef QT_RELEASE
#define FATAL(v1, v2, ...)              { Debugger::Fatal(__FILE__, __LINE__, __PRETTY_FUNCTION__, ANSI_TEXT_BLUE, ANSI_TEXT_RED, v1, ##__VA_ARGS__); exit(v1); }
#endif 



#define DEBUG_COLOR    ANSI_RESET
#define INFO_COLOR     ANSI_TEXT_GREEN
#define WARNING_COLOR  ANSI_TEXT_YELLOW
#define CRITICAL_COLOR ANSI_TEXT_MAGENTA
#define FATAL_COLOR    ANSI_TEXT_RED

#define LINE_ENDING "\n"


#ifndef __DEBUGGER_H
#define __DEBUGGER_H

#define BUFFER_SIZE 65536

namespace Debugger
{
	enum OutType : quint8
	{
		OUT_DEBUG    = 1,
		OUT_INFO     = 2,
		OUT_WARNING  = 4,
		OUT_CRITICAL = 8,
		OUT_FATAL    = 16
	};

	extern OutType out_type;
	extern QTemporaryFile* file_to_write;
	extern QTextStream* f;
	extern QTextStream* term_to_write;
	extern QTextStream* err_to_write;
	extern char s1[BUFFER_SIZE];
	extern QString s2;

	/*        filename          line    function name     info color       text color       message         args */
	void Debug(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...);
	void Info(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...);
	void Warning(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...);
	void Critical(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...);
	void Fatal(const char* _s1, int _l, const char* _s2,  const char* _s3, const char* _s4, const char* _s5, ...);

	/* Message Handler */
	void MessageHandler(QtMsgType _t, const QMessageLogContext& _c, const QString& _m);
	void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
}

#endif //__DEBUGGER_H

